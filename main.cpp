/**************************************************************************
**  File: main.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#include "widget.h"
#include <QApplication>





int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    Widget w;
    w.show();


    return a.exec();
}
