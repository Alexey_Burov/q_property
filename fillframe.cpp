/**************************************************************************
**  File: fillframe.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#include "fillframe.h"
#include <QPushButton>

#include <QDebug>

FillFrame::FillFrame(QWidget *parent) :
    QFrame(parent)
{

}

int FillFrame::getFill()
{
    return fill;
}

void FillFrame::setFill(int value)
{
    fill = value;
    repaint();
}
