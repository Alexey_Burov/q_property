/**************************************************************************
**  File: fillframe.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#ifndef FILLFRAME_H
#define FILLFRAME_H

#include <QFrame>

class FillFrame : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(int fill READ getFill WRITE setFill)
public:
    explicit FillFrame(QWidget *parent = 0);

    /**
     * @brief getFill извлечение свойства fill
     */
    int getFill();

    /**
     * @brief setFill установка свойства fill
     */
    void setFill(int value);

signals:

public slots:

protected:
   int fill;

};

#endif // FILLFRAME_H
