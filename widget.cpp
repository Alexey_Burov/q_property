/**************************************************************************
**  File: widget.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFile>

namespace  {
static QString qssStr;
}

static bool readStyle(){
    QFile styleF;
    styleF.setFileName(":/qss/styles.qss");
    if(styleF.open(QFile::ReadOnly)){
        qssStr = styleF.readAll();
        return true;
    }
    else{
        qssStr = "";
        return false;
    }
}

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->frame->setProperty("fill",1);
    connect(ui->spinBox,SIGNAL(valueChanged(int)),this,SLOT(setFillProperty(int)));

    if(::readStyle())
        qApp->setStyleSheet(qssStr);

}

Widget::~Widget()
{
    delete ui;
}

void Widget::setFillProperty(int value)
{
    if(::qssStr.isEmpty())
        return;

    ui->frame->setProperty("fill", value);
    qApp->setStyleSheet(qssStr);

}
