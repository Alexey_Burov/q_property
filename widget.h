/**************************************************************************
**  File: widget.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include "fillframe.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

private slots:


    void setFillProperty(int value);

};

#endif // WIDGET_H
