#-------------------------------------------------
#
# Project created by QtCreator 2019-08-21T10:26:40
#
#-------------------------------------------------

CONFIG += c++11

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qssproject
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    fillframe.cpp

HEADERS  += widget.h \
    fillframe.h

FORMS    += widget.ui

RESOURCES += \
    res.qrc
